# StoryTeller
A framework to create simple story-telling apps with BGM, Q&A, images and narration.

## Features

StoryTeller allows creating stories from *panels*, which are basically slides containing images and a bit of narration.
Panels have multiple features, such as:

### Background Music

For each panel, a BGM url can be provided in order to play the BGM. It's enough to specify the BGM for only one panel, as the others will keep playing the track.
The track loops forever.

When specifying a BGM for two panels, when loading the panel with a different BGM url the current music will fade out, and the newly-loaded track will fade-in, transitioning seamlessly between tracks.

### Background Images

Each panel has the option to include a background image to illustrate the narration in the narration box.

### Questions

Questions can be included in a panel. By default, a maximum of 4 questions are allowed. The reader will not be able to progress until he answers correctly to a question.
Multiple correct answers are allowed.

Panels with questions must have some narration as well, as the questions are presented only the panel narration is displayed.

### Character dialogs

By specifying a `speaker` in the panel configuration options, that panel will have a box indicating the name of the speaker.
Using this, dialogs between characters are possible to simulate.

## Instalation & Usage

### Dependencies

StoryTeller relies on having FontAwesome and the Google provided Pangolin font. Be sure to have something like this in your `<head>`:
```
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Pangolin" rel="stylesheet">
```

**This is just an example, be sure to grab the latest versions of the dependencies from appropriate sources**

Supported browsers are latest versions of FF, Chrome, Opera and Safari. Support for IE/Edge is not provided, although it should work on these browsers with minimal issues.

### Demo

A basic demo is available under the `index.html` file in the main repo.

### Instalation

In order to use StoryTeller, include the following link in your `<head>`:
```
<link rel="stylesheet" type="text/css" media="screen" href="path/to/storyteller/css/storyteller.style.css" />
```

and the following script declarations at the bottom of the `<body?`:
```
<script src="js/storyteller.panel.js"></script>
<script src="js/storyteller.js"></script>
```

### Usage

Create a new instance of the StoryTeller:

```
var storyteller = new StoryTeller("My special story");
```

You can provide a container id upon instantiation for the markup that will be inserted:

```
var storyteller = new StoryTeller("My special story", 'myContainerId`);
```
If no container is provided, by default the markup will be inserted in the body.

Add some panels to the storyteller:

```
storyteller
	.addPanel({
	text: "One day, David Bowie decided to visit a country in the far east",
	bgURL: 'img/bg1.jpg',
	bgmURL: "audio/bgm.mp3"
	})
	.addPanel({
	speaker: "David Bowie",
	text: "Man, this looks like a nice metro",
	bgURL: 'img/bg1.jpg'                   
	});
```

or add them all at once:

```
storyteller
	.addPanels([{
			text: "One day, David Bowie decided to visit a country in the far east",
			bgURL: 'img/bg1.jpg',
			bgmURL: "audio/bgm.mp3"
		},
		{
			speaker: "David Bowie",
			text: "Man, this looks like a nice metro",
			bgURL: 'img/bg1.jpg'                   
	}]);
```

Initialize the storyteller:

```
storyteller.init();
```

And that's it!

### Panel configuration options

The following options are available for panerl configuration:

```
panelOptions - the object containing the options to instantiate the panel
panelOptions.text - the text of the panel. 
Used for explaining what's going on in the current panel.
panelOptions.speaker - the character speaking. May be undefined, indicating that it's the narrator speaking 
panelOptions.bgURL - the URL for the background image of the panel
panelOptions.bgmURL - the URL for the background music that will be played on this panel. 
If empty, previous BGM will play. 
None will stop the current BGM from playing.
panelOptions.question - the object that contains the questions related to the current panel.
If undefined, panel will not have any questions.
panelOptions.question.text - the text of the question
panelOptions.question.answers - the answers to the question. Max allowed answers - 4
panelOptions.question.answers[].text - the text of the answer
panelOptions.question.answers[].correct - Any truthie value indicates the answer is correct.
```

### Docs
Extended documentation available as JSDocs.

## TODOs

Add an option panel to restart the story and start/stop the music

*TBA*