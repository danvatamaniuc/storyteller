/**
* StoryTeller class used to compose a story with interactive elements,
* such as audio, video and even questions with one correct answer
* @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
* @since 2018-05-05
* @version 1.0.0
* @class
* @param {string} title - Title of the story
* @param {string} containerId - id of the container in which the story should be created.
* Leave undefined for the story markup to be introduced directly in the body
* @returns {Object}
*/

function StoryTeller(title, containerId) {

    /**
    * CSS class of the storyteller panels container 
    * @name mainContainerClass
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    var _mainContainerClass = 'storyteller-main'
    /**
    * CSS class of the container for the title of the story
    * @name titleContainerClass
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _titleContainerClass = 'storyteller-title'
    /**
    * CSS class of the parent container of the storyteller
    * @name storyTellerContainerClass
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _storyTellerContainerClass = 'storyteller-container'
    /**
    * CSS class of the panel container
    * @name panelContainerClass
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _panelContainerClass = 'storyteller-panel'
    /**
    * Id of the button that starts the story
    * @name startButtonId
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _startButtonId = 'storyteller-start'
    /**
    * HTML markup for the button that starts the story
    * @name buttonContainer
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _buttonContainer =
        `<a href="#" id="${_startButtonId}"> 
            Start Story 
        </a>`
    /**
    * HTML markup for the title container
    * @name titleContainer
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _titleContainer =    
        `<div class="${_titleContainerClass}"></div>`
    /**
    * HTML markup for the content of the title container
    * @name titleContainerContent
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _titleContainerContent =
        `<h1>${title}</h1> 
        ${_buttonContainer}`
    /**
    * CSS class for the "previous" navigation arrow
    * @name navPreviousId
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _navPreviousId = 'storyteller-nav-prev'
    /**
    * HTML markup for the "previous" navigation arrow
    * @name navPreviousContainer
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _navPreviousContainer =
        `<a href="#" id="${_navPreviousId}">
            <i class="fas fa-chevron-left"></i>
        </a>`
        /**
    * CSS class for the "next" navigation arrow
    * @name navNextId
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _navNextId = 'storyteller-nav-next'
    /**
    * HTML markup for the "next" navigation arrow
    * @name navNextContainer
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _navNextContainer =
        `<a href="#" id="${_navNextId}">
            <i class="fas fa-chevron-right"></i>
        </a>`
    /**
    * HTML markup for the main container of the storyteller
    * @name mainContainer
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _mainContainer =
        `<div class="${_mainContainerClass}">
            ${_navPreviousContainer}
            ${_navNextContainer}
            ${_titleContainer}
        </div>`
    /**
    * Audio Object used to loop and play BGM
    * @name audio
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {Object}
    */
    , _audio
    /**
    * Changes the currently playing music, if any
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @name changeMusic
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTeller
    * @param {string} url - a valid url for the audio source
    * @returns {undefined}
    */
    , _changeMusic = function(url) {
        var self = this;

        if (!url) {
            _audio = null;
        }

        if (_audio) {
            _audioVolumeOut.call(this, _audio);
        }

        _audio = new Audio(url);
        // _audio.addEventListener('ended', function() {
        //     this.currentTime = 0;
        //     this.play();
        // }, false);
        _audioVolumeIn.call(this, _audio);
        _audio.play();
        _audio.onended = function () {
            self.nextPanel();
        }
    }
    /**
    * Fades a soundtrack in from 0 volume to current set volume
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @name audioVolumeIn
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTeller
    * @param {Object} audio - the audio object that needs to be faded in
    * @returns {undefined}
    */
    , _audioVolumeIn = function(audio){
        if(audio.volume){
           var InT = 0;
           var setVolume = this.volume;
           var speed = 0.05;
           audio.volume = InT;
           var eAudio = setInterval(function(){
               InT += speed;
               audio.volume = InT.toFixed(1);
               if(InT.toFixed(1) >= setVolume){
                  clearInterval(eAudio);
               };
           },50);
        };
    }
    /**
    * Fades a soundtrack out from current volume to 0, after which nulls the object
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @name audioVolumeOut
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTeller
    * @param {Object} audio - the audio object that needs to be faded in
    * @returns {undefined}
    */
    , _audioVolumeOut = function(audio){
        if(audio.volume){
           var InT = 0 + this.volume;
           var setVolume = 0;
           var speed = 0.05;
           audio.volume = InT;
           var fAudio = setInterval(function(){
               InT -= speed;
               audio.volume = InT.toFixed(1);
               if(InT.toFixed(1) <= setVolume){
                  clearInterval(fAudio);
                  audio.pause();
                  audio = null;
               };
           },50);
        };
    }
    /**
    * Setup function to add necessary classes to parent containers and other storyteller-exclusive markup,
    * such as navigation and options
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @name setup
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @returns {undefined}
    */
    , _setup = function() {
        var providedContainer = document.getElementById(containerId)
        , self = this
        ;
        
        if (!providedContainer) {
            providedContainer = document.body;
            document.getElementsByTagName("html")[0].className += " " + _storyTellerContainerClass + "-html";
        }

        providedContainer.className += " " + _storyTellerContainerClass;
        providedContainer.innerHTML = _mainContainer;

        document.getElementById(_navNextId).addEventListener('click', function() {
            self.nextPanel();
        });

        document.getElementById(_navPreviousId).addEventListener('click', function() {
            self.previousPanel();
        }); 
    }
    , listensKey = false
    ;

    /**
    * The panels of the storyteller
    * @name panels
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @memberof StoryTeller
    * @version 1.0.0
    * @type {Object[]}
    */
    this.panels = [];

    /**
    * The current panel object being displayed on the screen
    * @name currentPanel
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @memberof StoryTeller
    * @version 1.0.0
    * @type {Object}
    */
    this.currentPanel;

    /**
    * The current sound volume of the app
    * @name volume
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {Number}
    */
    this.volume = 0.7;

    /**
    * The index of the current panel in the list of panels
    * @name progress
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {Object}
    */
    this.progress;

    /**
    * Adds a panel to the storyteller. The order of the panels is the order of
    * their insertion in the storyteller.
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @param {Object}   panelOptions - the object containing the options to instantiate the panel
    * @returns {Object} The StoryTeller object, to allow chaining
    */
    this.addPanel = function(panelOptions) {
        this.panels.push(panelOptions);
        return this;
    }

    /**
    * Adds multiple panels to the storyteller. Order of panels is order of the panel options in array provided
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @param {Object[]} panelsOptions - Array of panelOption objects used to build panels
    * @returns {Object} The StoryTeller object, to allow chaining
    */
    this.addPanels = function(panelsOptions) {
        for (var i = 0; i < panelsOptions.length; i++) {
            this.addPanel(panelOptions);            
        }

        return this;
    }

    /**
    * Switches to the next panel. If no panels are available, the story resets to the title screen
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @returns {undefined}
    */
    this.nextPanel = function() {
        if (this.progress < 0) {
            self.progress = -1;
            document.getElementsByClassName(_titleContainerClass)[0].className = _panelContainerClass;
        }

        if (this.currentPanel && !this.currentPanel.questionDisplayed) {
            this.currentPanel.toggleQuestion();
            return;
        }

        if (this.currentPanel && !this.currentPanel.canContinue) {
            return;
        }

        this.progress += 1;

        if (this.progress >= this.panels.length) {
            this.init();
            _changeMusic();
            return;
        }

        this.currentPanel = new StoryTellerPanel(this.panels[this.progress]);
        this.currentPanel.display();        

        if (this.panels[this.progress].bgmURL) {
            _changeMusic.call(this, this.panels[this.progress].bgmURL);
        }
    }

    /**
    * Switches to the previous panel. If it's the first panel, resets to title screen
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @returns {undefined}
    */
    this.previousPanel = function() {
        this.progress -= 1;

        if (this.progress < 0) {
            this.init();
            return;
        }

        this.currentPanel = new StoryTellerPanel(this.panels[this.progress]);
        this.currentPanel.display();   

        if (this.panels[this.progress].bgmURL) {
            _changeMusic.call(this, this.panels[this.progress].bgmURL);
        }
    }

    /**
    * Initializes the story to the title screen
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @returns {undefined}
    */
    this.init = function() {
        var titleContainer = document.getElementsByClassName(_titleContainerClass)[0]
        , self = this
        ;

        if (!titleContainer) {
            titleContainer = document.getElementsByClassName(_panelContainerClass)[0];
            titleContainer.className = _titleContainerClass;
        }

        titleContainer.innerHTML = _titleContainerContent;

        this.progress = -1;

        document.getElementById(_startButtonId).addEventListener('click', function() {
            self.nextPanel();
        });

        if (!listensKey) {
            listensKey = true;
            document.addEventListener("keydown", function(event) {
                e = event || window.event;
                
                e.which === 37 && self.previousPanel();
                e.which === 39 && self.nextPanel();
            });
        }
    }

    /**
    * Highlights all specified words in the panels' text
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-06-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @param {String[]} words - list of words to be highlighted
    * @returns {undefined} No return value
    */
    this.highlight = function(words) {
        for (var i = 0; i < this.panels.length; i++) {

            for (let j = 0; j < words.length; j++) {
                const word = words[j];
                this.panels[i].text = this.panels[i].text.replace(word, `<span class="highlighted">${word}</span>`);            
            }

        }
    }

    _setup.call(this);
}