/**
* StoryTeller Panel class used to generate panel object that can be used to display
* the next item of the story and check the understanding of it by posing a question
* @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
* @since 2018-05-05
* @version 1.0.0
* @class
* @param {Object}   panelOptions - the object containing the options to instantiate the panel
* @param {string}   panelOptions.text - the text of the panel. 
* Used for explaining what's going on in the current panel.
* @param {string}   panelOptions.speaker - the character speaking. May be undefined, indicating that it's the narrator speaking 
* @param {string}   panelOptions.bgURL - the URL for the background image of the panel
* @param {string}   panelOptions.bgmURL - the URL for the background music that will be played on this panel. 
* If empty, previous BGM will play. 
* None will stop the current BGM from playing.
* @param {Object}   panelOptions.question - the object that contains the questions related to the current panel.
* If undefined, panel will not have any questions.
* @param {string}   panelOptions.question.text - the text of the question
* @param {Object[]} panelOptions.question.answers - the answers to the question. Max allowed answers - 4
* @param {string}   panelOptions.question.answers[].text - the text of the answer
* @param {string}   panelOptions.question.answers[].correct - Any truthie value indicates the answer is correct.
* Multiple correct answers are allowed.
* @returns {Object}
*/
function StoryTellerPanel(panelOptions) {
    /**
    * CSS class of the storyteller panels container 
    * @name mainContainerClass
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    var _mainContainerClass = 'storyteller-main'
    /**
    * CSS class of the panel container
    * @name panelContainerClass
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTeller
    * @type {string}
    */
    , _panelContainerClass = 'storyteller-panel'
    /**
    * CSS class of the narration container which holds the speaker and the text of the panel
    * @name narrationClass
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {string}
    */
    , _narrationClass = `${_panelContainerClass}-narration`
    /**
    * CSS id for the button that toggles the questions container
    * @name questionTogglerId
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {string}
    */
    , _questionTogglerId = `${_panelContainerClass}-toggle-questions`
    /**
    * CSS id for the answer buttons
    * @name answerId
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {string}
    */
    , _answerId = "answer"
    /**
    * CSS class for the rows of questions
    * @name _questionRowClass
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {string}
    */
    , _questionRowClass = `${_panelContainerClass}-question-row`
    /**
    * HTML markup for the content of the narration box
    * @name narrationContent
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {string}
    */
    , _narrationContent = 
        `${panelOptions.speaker ? `<span class="storyteller-speaker">${panelOptions.speaker}</span>`: ''}
        <p>${panelOptions.text}</p>`
    /**
    * HTML markup for the content of the panel
    * @name panelContent
    * @const
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {string}
    */
    , _panelContent =
        `${(panelOptions.bgURL ? `<img src="${panelOptions.bgURL}" />` : '')}
        <div class="${_narrationClass + ' ' + ((panelOptions.question && panelOptions.question.image) ? 'image-question' : '')}">
            ${_narrationContent}
        </div>`
        // ${panelOptions.question ? `<a href="#" id="${_questionTogglerId}">Questions</a>`: ''}`
    /**
    * HTML markup for the content of the question box
    * @name questionContent
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {string}
    */
    , _questionContent = panelOptions.question ? (panelOptions.question.image ?
        `<p>${panelOptions.question.text}</p>
        <div class="${_questionRowClass}">
            ${panelOptions.question.answers[0] ? `<a href="#" class="${_answerId}"><img data-answer="0" src="${panelOptions.question.answers[0].text}"></a>` : ''}
            ${panelOptions.question.answers[1] ? `<a href="#" class="${_answerId}"><img data-answer="1" src="${panelOptions.question.answers[1].text}"></a>` : ''}
        </div>
        <div class="${_questionRowClass}">
            ${panelOptions.question.answers[2] ? `<a href="#" class="${_answerId}"><img data-answer="2" src="${panelOptions.question.answers[2].text}"></a>` : ''}
            ${panelOptions.question.answers[3] ? `<a href="#" class="${_answerId}"><img data-answer="3" src="${panelOptions.question.answers[3].text}"></a>` : ''}
        </div>
        `
        :
        `<p>${panelOptions.question.text}</p>
        <div class="${_questionRowClass}">
            ${panelOptions.question.answers[0] ? `<a href="#" class="${_answerId}" data-answer="0">${panelOptions.question.answers[0].text}</a>` : ''}
            ${panelOptions.question.answers[1] ? `<a href="#" class="${_answerId}" data-answer="1">${panelOptions.question.answers[1].text}</a>` : ''}
        </div>
        <div class="${_questionRowClass}">
            ${panelOptions.question.answers[2] ? `<a href="#" class="${_answerId}" data-answer="2">${panelOptions.question.answers[2].text}</a>` : ''}
            ${panelOptions.question.answers[3] ? `<a href="#" class="${_answerId}" data-answer="3">${panelOptions.question.answers[3].text}</a>` : ''}
        </div>
        `) : ''

    /**
    * Displays the narration in the narration box
    * @name _displayNarration
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @returns {undefined}
    */
    , _displayNarration = function() {
        var narrationContainer = document.getElementsByClassName(_narrationClass)[0]
        ;

        narrationContainer.innerHTML = _narrationContent;
    }
    , _displayQuestion = function() {
        var narrationContainer = document.getElementsByClassName(_narrationClass)[0]
        , answerElements
        , self = this
        ;

        narrationContainer.innerHTML = _questionContent;
     
        answerElements = document.getElementsByClassName(_answerId);

        for (let i = 0; i < answerElements.length; i++) {
            let element = answerElements[i];
            
            element.addEventListener('click', function(event) {
                let answerId = event.target.dataset.answer
                    , url = 'audio/correct.mp3'
                ;

                if (panelOptions.question.answers[answerId] && panelOptions.question.answers[answerId].correct) {
                    self.canContinue = true;
                    panelOptions.question.image ? event.target.parentNode.className += " correct" : event.target.className += " correct";
                } else {
                    panelOptions.question.image ? event.target.parentNode.className += " wrong" : event.target.className += " wrong";
                    url = 'audio/wrong.mp3';
                }

                _audio = new Audio(url);
                _audio.play();
            });
        }
    }
    ;

    /**
    * Indicates if progression to next slide is possible
    * @name canContinue
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {boolean}
    */
    this.canContinue = !panelOptions.question;

    /**
    * Indicates if the question is currently displayed.
    * Useful if the question has to be displayed from outside (such as the next/previous buttons)
    * @name questionDisplayed
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @type {boolean}
    */
    this.questionDisplayed = !panelOptions.question;

    /**
    * Clears the storyteller container and displays the new panel instead
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-05
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @returns {undefined}
    */
    this.display = function() { 
        var panelContainer = document.getElementsByClassName(_panelContainerClass)[0]
        , self = this
        , audio
        ;

        panelContainer.innerHTML = _panelContent;

        if (panelOptions.question) {
            document.getElementById(_questionTogglerId).addEventListener('click', function() {
                self.toggleQuestion();
            });
        }
    }

    /**
    * Displays/Removes the question from the panel in the narration container
    * @author Vatamaniuc Dan-Alexandru <d.a.vatamaniuc@gmail.com>
    * @since 2018-05-06
    * @version 1.0.0
    * @memberof StoryTellerPanel
    * @returns {undefined}
    */
    this.toggleQuestion = function() {
        if (this.questionDisplayed) {
            _displayNarration.call(this);
        } else {
            _displayQuestion.call(this);
        }

        this.questionDisplayed = !this.questionDisplayed;
    }
}